<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Task;
use Tests\TestingData;
use App\Services\ErrorMessage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiEndpointResponseTest extends TestCase
{
    use RefreshDatabase, TestingData;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    /** @test */
    public function check_login_endpoint_fields_validation()
    {
        //case 1 - username fields is required
        $response = $this->json('post', '/api/login', [
            // 'username' => 'admin',
            'password' => 'password',
        ]);
        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['username']));

        //case 2 - username needs to be min 2 symbol
        $response = $this->json('post', '/api/login', [
            'username' => 'a',
            'password' => 'password',
        ]);
        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['username']));

        //case 3 - username needs to be max 50 symbol
        $response = $this->json('post', '/api/login', [
            'username' => 'adminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadmin',
            'password' => 'password',
        ]);
        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['username']));

        //case 4 - password fields is required
        $response = $this->json('post', '/api/login', [
            'username' => 'admin',
            // 'password' => 'password',
        ]);
        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['password']));

        //case 5 - password needs to be min 6 symbol
        $response = $this->json('post', '/api/login', [
            'username' => 'admin',
            'password' => 'p',
        ]);
        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['password']));

        //case 6 - password needs to be max 50 symbol
        $response = $this->json('post', '/api/login', [
            'username' => 'admin',
            'password' => 'passwordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpassword',
        ]);
        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['password']));
    }

    /** @test */
    public function check_login_authorization_functionality()
    {
        //create user
        $user = $this->users(['username' => 'admin'])->first();

        //case 1 - if username is wrong
        $response = $this->json('post', '/api/login', [
            'username' => 'adminn',
            'password' => 'password',
        ]);
        $response->assertStatus(401);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals("Unauthorized", $data['error']);

        //case 2 - if password is wrong
        $response = $this->json('post', '/api/login', [
            'username' => 'admin',
            'password' => 'passwordd',
        ]);
        $response->assertStatus(401);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals("Unauthorized", $data['error']);

        //case 3 - if password and username is correct it should return authorized user with access token
        $response = $this->json('post', '/api/login', [
            'username' => 'admin',
            'password' => 'password',
        ]);
        $response->assertStatus(200);
        //check response structure
        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in',
            'user' => [
                'id',
                'name',
                'username',
                'created_at',
            ],
        ]);
        $data = json_decode($response->getContent(), true);

        //check if access token is valid
        $response = $this->json('GET', '/api/me', [], [
            'Authorization' => 'Bearer ' . $data['access_token'],
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'username',
                'created_at',
            ]
        ]);
    }

    /** @test */
    public function check_tasks_endpoint()
    {
        //unauthorized user can't access this endpoint
        $response = $this->json('GET', '/api/tasks', []);
        $response->assertStatus(401);
        //login as jwt user
        $this->jwtUser();
        //create some tasks
        $this->tasks([], 5);
        $response = $this->json('GET', '/api/tasks', []);
        $response->assertStatus(200);
        $data = json_decode($response->getContent())->data;
        $this->assertEquals(count($data), 5);
    }

    /** @test */
    public function check_single_task_endpoint()
    {
        //unauthorized user can't access this endpoint
        $response = $this->json('GET', '/api/tasks', []);
        $response->assertStatus(401);
        //login as jwt user
        $this->jwtUser();
        //create some tasks
        $task = $this->tasks([], 1)->first();
        $response = $this->json('GET', '/api/tasks/' . $task->id . '/show', []);
        $response->assertStatus(200);
    }

    /** @test */
    public function check_created_tasks_endpoint()
    {
        //unauthorized user can't access this endpoint
        $response = $this->json('GET', '/api/me/tasks/created', []);
        $response->assertStatus(401);
        //login as jwt user
        $user = $this->jwtUser();

        //create some tasks as logged in user
        $this->tasks(['author_id' => $user->id], 5);
        //create sone tasks as other user
        $this->tasks([], 5);

        $response = $this->json('GET', '/api/me/tasks/created', []);

        $response->assertStatus(200);
        $data = json_decode($response->getContent())->data;
        $this->assertEquals(count($data), 5);
    }

    /** @test */
    public function check_assigned_tasks_endpoint()
    {
        //unauthorized user can't access this endpoint
        $response = $this->json('GET', '/api/me/tasks/assigned', []);
        $response->assertStatus(401);
        //login as jwt user
        $user = $this->jwtUser();

        //create some tasks as logged in user
        $this->tasks(['assigned_user_id' => $user->id], 5);
        //create sone tasks as other user
        $this->tasks([], 5);

        $response = $this->json('GET', '/api/me/tasks/assigned', []);

        $response->assertStatus(200);
        $data = json_decode($response->getContent())->data;
        $this->assertEquals(count($data), 5);
    }

    /** @test */
    public function check_create_tasks_endpoint()
    {
        //unauthorized user can't access this endpoint
        $response = $this->json('POST', '/api/me/tasks/crete', []);
        $response->assertStatus(401);
        //login as jwt user
        $user = $this->jwtUser();

        $response = $this->json('POST', '/api/me/tasks/crete', []);
        $response->assertStatus(422);
    }

    /** @test */
    public function check_create_task_validation()
    {
        //login as jwt user
        $user = $this->jwtUser();

        //case 1 - name fields is required
        $response = $this->json('POST', '/api/me/tasks/crete', [
            // 'name' => 'name',
            'text' => 'text',
            'assigned_user_id' => $user->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['name']));

        //case 2 - name field min 2 symbol
        $response = $this->json('POST', '/api/me/tasks/crete', [
            'name' => 'n',
            'text' => 'text',
            'assigned_user_id' => $user->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['name']));

        //case 3 - name field max 255 symbol
        $response = $this->json('POST', '/api/me/tasks/crete', [
            'name' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas, quaerat dignissimos! Laudantium tenetur enim architecto cum, est eum at modi facere natus nam voluptate quidem quae deserunt excepturi maxime error ipsum rem impedit quas. Vero voluptatibus in ex assumenda? Suscipit illum voluptatum at quos et dolor aliquam nulla asperiores. Inventore voluptate dolor, sapiente facere officiis magnam! Eum quod iure inventore odit error dolore voluptatem numquam temporibus fuga, quia beatae dolores, repellat a! Facere ducimus esse iste exercitationem id iure harum, sequi non corporis officiis vel nihil. Optio nam fugit assumenda atque quia similique reprehenderit expedita in beatae explicabo sapiente minus corporis, odio sint inventore, delectus natus ullam! Similique quam quasi unde, eveniet iure praesentium aperiam expedita quos! Libero placeat vero tempora recusandae iusto animi, voluptate sint neque ex iure, minus minima voluptatibus at aut aliquam nesciunt. Eligendi neque non nesciunt, dolorum facere rerum. Accusantium deserunt delectus magni ipsam molestias earum, voluptate quam quisquam similique quibusdam aliquam repudiandae asperiores commodi architecto facere optio non officia ipsum eligendi id labore eos alias quis dicta! Iste error esse voluptates ut, quia ab maiores, excepturi distinctio dicta hic necessitatibus accusantium! Rem cumque quibusdam veritatis perspiciatis debitis, odio soluta tempore vitae sint repudiandae mollitia ut! Quas cumque ab dolor pariatur sed vitae qui possimus id officiis saepe doloremque natus animi, hic iste omnis mollitia. Eveniet commodi dolore ex voluptas, corrupti voluptatem neque quam quo corporis dolor soluta dolorum deserunt libero harum, facilis error rerum. Consequatur eaque incidunt minus fuga nihil doloribus dolore nisi aperiam cupiditate placeat enim voluptates, molestiae, odio a similique recusandae accusamus alias?',
            'text' => 'text',
            'assigned_user_id' => $user->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['name']));



        //case 4 - name fields is required
        $response = $this->json('POST', '/api/me/tasks/crete', [
            'name' => 'name',
            // 'text' => 'text',
            'assigned_user_id' => $user->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['text']));

        //case 5 - name field min 2 symbol
        $response = $this->json('POST', '/api/me/tasks/crete', [
            'name' => 'name',
            'text' => 't',
            'assigned_user_id' => $user->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['text']));

        //case 6 - name field max 1000 symbol
        $response = $this->json('POST', '/api/me/tasks/crete', [
            'name' => 'name',
            'text' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis iusto ipsam, facere rem illo aperiam aspernatur nobis inventore assumenda obcaecati minima soluta voluptatum laborum in quam libero alias ipsa expedita! Ab aliquam quam, mollitia beatae modi esse fugit inventore ratione id sequi neque, omnis eaque, temporibus pariatur ut nobis repellendus. Vitae, necessitatibus. Voluptas incidunt dolorum perspiciatis tenetur et voluptate reiciendis quasi reprehenderit, tempore nulla, commodi atque dignissimos sapiente quos cum? Quam ut, provident iusto odit placeat dolores saepe accusamus illo repudiandae aut earum natus itaque voluptas hic quasi quae quisquam corrupti adipisci! Accusantium accusamus praesentium rem! Explicabo voluptates dolorum quo repellat in odit facilis quibusdam suscipit soluta cum! Suscipit molestias incidunt ea minus voluptate dolorem quibusdam deleniti nemo nesciunt nisi similique minima esse assumenda veniam illum maiores libero eveniet laudantium animi, sapiente officia officiis id. Sunt laboriosam inventore quam soluta, consequatur tempore a fugiat et? Ad odio eligendi aliquid, quisquam aliquam ex magnam debitis voluptas voluptatum natus cupiditate minima ipsa hic error nemo at voluptates perferendis quia reprehenderit laboriosam repellendus quasi! Exercitationem mollitia commodi impedit iure odit sed provident, magnam hic tempore rerum animi quaerat odio rem soluta consequuntur illum iste repellat saepe! Perferendis numquam tempora aut eligendi a! Sequi ab voluptatibus deserunt, blanditiis excepturi magnam quae officia eligendi ipsa tenetur quia magni odit eaque doloremque nihil incidunt, repellat totam sapiente. Quidem placeat ut aut iure esse recusandae quo molestiae fuga sit consequuntur minus, repudiandae laboriosam dicta reprehenderit obcaecati nam soluta quod incidunt harum optio aspernatur? Quae tempore explicabo laudantium, possimus repellendus officiis totam repellat aperiam quis? Aspernatur ut dolor saepe facere aliquid cum earum. Soluta, optio? Quaerat blanditiis ducimus recusandae dolore commodi dolor quasi repudiandae ab. Asperiores soluta tenetur quod exercitationem, recusandae impedit at possimus! Quos ratione consequuntur ducimus expedita, voluptates aliquid itaque debitis natus alias sed maiores nisi repellendus beatae aperiam incidunt, maxime doloribus repudiandae exercitationem neque. Modi corporis inventore, nobis mollitia numquam quaerat eum sunt voluptatibus est, fuga nam odit atque? Impedit illo saepe labore at totam odit sunt cumque tempora ratione porro minus facere voluptatibus minima eum dignissimos, quae quos sequi hic ipsa, earum officiis rerum veritatis assumenda. Quis delectus a laboriosam ut, possimus incidunt explicabo. Vel error maxime dignissimos quia non eveniet iste odio dolor ipsa, voluptate nihil aspernatur. Quod maiores aspernatur architecto deleniti repellendus, eius laborum pariatur, et nostrum labore, eum aliquid corrupti aut qui odio eveniet! Repellendus autem voluptatem quaerat laboriosam dicta corrupti molestias, sint blanditiis sunt ratione saepe? Iusto ipsum illum enim aliquam quam maiores animi quidem? Odit, saepe suscipit eligendi obcaecati nam animi similique autem, soluta voluptatibus earum in, quasi qui exercitationem eum? Ipsa nisi, facere id aperiam reprehenderit aut eos odio sint est, pariatur voluptatem! At officiis, ex odit rem repudiandae autem laborum earum soluta rerum accusantium obcaecati ullam eius quos velit cupiditate recusandae! Facere excepturi architecto consequuntur, cupiditate, molestiae consequatur repellat reprehenderit molestias non quis sunt commodi, rerum facilis repellendus delectus fugit fuga? Aut id, dolor unde eveniet beatae temporibus iste, libero, optio tempore iusto facilis corporis illum rem doloremque iure aperiam ullam odio neque earum eaque reiciendis voluptas! Minima possimus quis illum laboriosam rerum consequatur mollitia, cumque nobis similique. Voluptatibus, et quaerat alias, unde debitis similique fuga, fugiat reiciendis inventore est beatae harum? Quo assumenda animi eos quibusdam reiciendis accusamus. Dolorum, ad! Libero nihil esse laborum eum nulla dicta illo veritatis, laboriosam explicabo blanditiis nam iusto eos maiores voluptates? Saepe eligendi, aliquid veritatis reiciendis vel placeat. Ex ipsa facilis illum asperiores labore suscipit mollitia laboriosam atque pariatur nihil saepe unde hic alias nostrum nulla, minus, illo ipsum deserunt officia ad soluta libero repellat dolorum. Voluptas iste hic blanditiis molestias! Consectetur autem ea, magni maxime vitae delectus hic. Et provident libero quisquam enim modi assumenda eveniet perspiciatis error corrupti. Enim aliquid suscipit molestias ratione nam! Facilis minima et illum delectus iste. Optio officiis aliquam earum molestiae reiciendis recusandae illo quia non, tenetur aperiam adipisci impedit autem quam quasi iste harum incidunt quibusdam odit architecto sint repellendus, assumenda maxime possimus placeat. Repudiandae dolorem illo, voluptatibus quibusdam ratione quasi recusandae fugiat atque! Nihil maiores fugiat natus molestiae iste magni tenetur optio vitae illo? Beatae fuga recusandae cum. Ad fuga soluta et earum. Cupiditate ad et molestias ipsam dolores minima deserunt numquam nisi totam sequi praesentium itaque explicabo at voluptate, repudiandae eum tempore illo iste dolorum est debitis dolorem. Reprehenderit nobis hic asperiores minima quo laudantium maxime fugit, commodi iure deserunt officia! Nisi nihil esse nesciunt quibusdam minus deleniti rem optio eum, iusto eaque autem fuga quod delectus doloremque sint, alias et, fugiat sapiente officia? Fugiat quia, consectetur non a blanditiis itaque rem vero architecto doloremque voluptatem, quibusdam sapiente temporibus iusto consequuntur. Animi ipsum voluptatibus suscipit, ipsam facilis odit eaque quis quos accusamus ducimus est, similique veritatis nostrum necessitatibus quisquam, perspiciatis unde ad iure aspernatur libero doloremque numquam in? Voluptatem veniam facere beatae! Quas repellendus vero repellat accusamus tempora, quisquam maiores vitae molestias. Accusantium nemo culpa hic saepe reprehenderit vitae porro, consectetur sapiente suscipit beatae ipsam. Tempora voluptatum cumque dignissimos fugiat tempore nesciunt vitae quasi ipsa id reiciendis harum qui, nostrum et eveniet recusandae provident sequi magni, aliquid alias animi culpa quia autem quibusdam. Inventore dolorem amet a maxime qui voluptas ullam, consequuntur exercitationem facilis tempora consectetur molestiae ex quo sed quidem, repellendus commodi quibusdam doloribus autem iure? Sapiente perferendis voluptatum assumenda ea! Cupiditate ducimus similique hic quidem, deleniti molestias eos eveniet libero a esse quaerat asperiores ipsam autem temporibus error minus expedita! Tenetur inventore facere nulla assumenda eos id in consequuntur at impedit? Sapiente, provident, accusantium distinctio quisquam commodi cumque eos hic facilis temporibus beatae voluptas sunt id quaerat quasi perferendis veritatis? Reprehenderit modi doloremque dolore distinctio fuga at, doloribus nostrum maxime commodi perspiciatis tenetur optio cumque eos nobis maiores sunt, velit veritatis nemo vero laudantium. Vero laborum excepturi rem reiciendis eligendi? Ad, molestiae nihil nisi earum voluptatem a commodi sunt quia dignissimos libero necessitatibus, deleniti nam laudantium autem dolor fugiat mollitia fuga inventore quam minima enim quasi? Sapiente, quasi animi quibusdam, tempore aspernatur earum mollitia voluptates libero laborum sunt culpa harum.',
            'assigned_user_id' => $user->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['text']));

        //case 7 - assigned_user_id fields is required
        $response = $this->json('POST', '/api/me/tasks/crete', [
            'name' => 'name',
            'text' => 'text',
            // 'assigned_user_id' => $user->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['assigned_user_id']));

        //case 8 - assigned_user_id fields needs to be exist in our table
        $response = $this->json('POST', '/api/me/tasks/crete', [
            'name' => 'name',
            'text' => 'text',
            'assigned_user_id' => 1111,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['assigned_user_id']));

        //case 8 - deadline fields is required
        $response = $this->json('POST', '/api/me/tasks/crete', [
            'name' => 'name',
            'text' => 'text',
            'assigned_user_id' => $user->id,
            // 'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['deadline']));


        //case 9 - deadline field format is wrong
        $response = $this->json('POST', '/api/me/tasks/crete', [
            'name' => 'name',
            'text' => 'text',
            'assigned_user_id' => $user->id,
            'deadline' => date("y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['deadline']));

        //case 10 - deadline needs to be min one hour after current date time
        $response = $this->json('POST', '/api/me/tasks/crete', [
            'name' => 'name',
            'text' => 'text',
            'assigned_user_id' => $user->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 10 minute")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['deadline']));

        //case 11 - it should create new task
        $deadline = date("Y-m-d\TH:i", strtotime("+ 2 Hour"));
        $response = $this->json('POST', '/api/me/tasks/crete', [
            'name' => 'name different',
            'text' => 'text different',
            'assigned_user_id' => $user->id,
            'deadline' => $deadline,
        ]);

        $response->assertStatus(201);

        $this->assertDatabaseHas('tasks', [
            'name' => 'name different',
            'text' => 'text different',
            'assigned_user_id' => $user->id,
            'deadline' => date("Y-m-d H:i:s", strtotime($deadline)),
        ]);
    }

    /** @test */
    public function check_update_task_validation()
    {
        //login as jwt user
        $userOne = $this->jwtUser();

        $userSecond = $this->users()->first();

        //create task for user second and check that user one cannot update post
        $userSecondTask = $this->tasks(['author_id' => $userSecond->id, 'status' => Task::STATUS_TO_DO])->first();
        $userOneTask = $this->tasks(['author_id' => $userOne->id, 'status' => Task::STATUS_TO_DO])->first();

        $deadline = date("Y-m-d\TH:i", strtotime("+ 2 Hour"));
        $response = $this->json('PUT', '/api/me/tasks/' . $userSecondTask->id . '/update', [
            'name' => 'name_',
            'text' => 'text_',
            'assigned_user_id' => $userOne->id,
            'deadline' => $deadline,
        ]);

        $response->assertStatus(403);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals(ErrorMessage::TASK_DOES_NOT_BELONGS_TO_USER, $data);

        //case 1 - name fields is required
        $response = $this->json('PUT', '/api/me/tasks/' . $userOneTask->id . '/update', [
            // 'name' => 'name',
            'text' => 'text',
            'assigned_user_id' => $userOne->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['name']));

        //case 2 - name field min 2 symbol
        $response = $this->json('PUT', '/api/me/tasks/' . $userOneTask->id . '/update', [
            'name' => 'n',
            'text' => 'text',
            'assigned_user_id' => $userOne->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['name']));

        //case 3 - name field max 255 symbol
        $response = $this->json('PUT', '/api/me/tasks/' . $userOneTask->id . '/update', [
            'name' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas, quaerat dignissimos! Laudantium tenetur enim architecto cum, est eum at modi facere natus nam voluptate quidem quae deserunt excepturi maxime error ipsum rem impedit quas. Vero voluptatibus in ex assumenda? Suscipit illum voluptatum at quos et dolor aliquam nulla asperiores. Inventore voluptate dolor, sapiente facere officiis magnam! Eum quod iure inventore odit error dolore voluptatem numquam temporibus fuga, quia beatae dolores, repellat a! Facere ducimus esse iste exercitationem id iure harum, sequi non corporis officiis vel nihil. Optio nam fugit assumenda atque quia similique reprehenderit expedita in beatae explicabo sapiente minus corporis, odio sint inventore, delectus natus ullam! Similique quam quasi unde, eveniet iure praesentium aperiam expedita quos! Libero placeat vero tempora recusandae iusto animi, voluptate sint neque ex iure, minus minima voluptatibus at aut aliquam nesciunt. Eligendi neque non nesciunt, dolorum facere rerum. Accusantium deserunt delectus magni ipsam molestias earum, voluptate quam quisquam similique quibusdam aliquam repudiandae asperiores commodi architecto facere optio non officia ipsum eligendi id labore eos alias quis dicta! Iste error esse voluptates ut, quia ab maiores, excepturi distinctio dicta hic necessitatibus accusantium! Rem cumque quibusdam veritatis perspiciatis debitis, odio soluta tempore vitae sint repudiandae mollitia ut! Quas cumque ab dolor pariatur sed vitae qui possimus id officiis saepe doloremque natus animi, hic iste omnis mollitia. Eveniet commodi dolore ex voluptas, corrupti voluptatem neque quam quo corporis dolor soluta dolorum deserunt libero harum, facilis error rerum. Consequatur eaque incidunt minus fuga nihil doloribus dolore nisi aperiam cupiditate placeat enim voluptates, molestiae, odio a similique recusandae accusamus alias?',
            'text' => 'text',
            'assigned_user_id' => $userOne->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['name']));



        //case 4 - name fields is required
        $response = $this->json('PUT', '/api/me/tasks/' . $userOneTask->id . '/update', [
            'name' => 'name',
            // 'text' => 'text',
            'assigned_user_id' => $userOne->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['text']));

        //case 5 - name field min 2 symbol
        $response = $this->json('PUT', '/api/me/tasks/' . $userOneTask->id . '/update', [
            'name' => 'name',
            'text' => 't',
            'assigned_user_id' => $userOne->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['text']));

        //case 6 - name field max 1000 symbol
        $response = $this->json('PUT', '/api/me/tasks/' . $userOneTask->id . '/update', [
            'name' => 'name',
            'text' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis iusto ipsam, facere rem illo aperiam aspernatur nobis inventore assumenda obcaecati minima soluta voluptatum laborum in quam libero alias ipsa expedita! Ab aliquam quam, mollitia beatae modi esse fugit inventore ratione id sequi neque, omnis eaque, temporibus pariatur ut nobis repellendus. Vitae, necessitatibus. Voluptas incidunt dolorum perspiciatis tenetur et voluptate reiciendis quasi reprehenderit, tempore nulla, commodi atque dignissimos sapiente quos cum? Quam ut, provident iusto odit placeat dolores saepe accusamus illo repudiandae aut earum natus itaque voluptas hic quasi quae quisquam corrupti adipisci! Accusantium accusamus praesentium rem! Explicabo voluptates dolorum quo repellat in odit facilis quibusdam suscipit soluta cum! Suscipit molestias incidunt ea minus voluptate dolorem quibusdam deleniti nemo nesciunt nisi similique minima esse assumenda veniam illum maiores libero eveniet laudantium animi, sapiente officia officiis id. Sunt laboriosam inventore quam soluta, consequatur tempore a fugiat et? Ad odio eligendi aliquid, quisquam aliquam ex magnam debitis voluptas voluptatum natus cupiditate minima ipsa hic error nemo at voluptates perferendis quia reprehenderit laboriosam repellendus quasi! Exercitationem mollitia commodi impedit iure odit sed provident, magnam hic tempore rerum animi quaerat odio rem soluta consequuntur illum iste repellat saepe! Perferendis numquam tempora aut eligendi a! Sequi ab voluptatibus deserunt, blanditiis excepturi magnam quae officia eligendi ipsa tenetur quia magni odit eaque doloremque nihil incidunt, repellat totam sapiente. Quidem placeat ut aut iure esse recusandae quo molestiae fuga sit consequuntur minus, repudiandae laboriosam dicta reprehenderit obcaecati nam soluta quod incidunt harum optio aspernatur? Quae tempore explicabo laudantium, possimus repellendus officiis totam repellat aperiam quis? Aspernatur ut dolor saepe facere aliquid cum earum. Soluta, optio? Quaerat blanditiis ducimus recusandae dolore commodi dolor quasi repudiandae ab. Asperiores soluta tenetur quod exercitationem, recusandae impedit at possimus! Quos ratione consequuntur ducimus expedita, voluptates aliquid itaque debitis natus alias sed maiores nisi repellendus beatae aperiam incidunt, maxime doloribus repudiandae exercitationem neque. Modi corporis inventore, nobis mollitia numquam quaerat eum sunt voluptatibus est, fuga nam odit atque? Impedit illo saepe labore at totam odit sunt cumque tempora ratione porro minus facere voluptatibus minima eum dignissimos, quae quos sequi hic ipsa, earum officiis rerum veritatis assumenda. Quis delectus a laboriosam ut, possimus incidunt explicabo. Vel error maxime dignissimos quia non eveniet iste odio dolor ipsa, voluptate nihil aspernatur. Quod maiores aspernatur architecto deleniti repellendus, eius laborum pariatur, et nostrum labore, eum aliquid corrupti aut qui odio eveniet! Repellendus autem voluptatem quaerat laboriosam dicta corrupti molestias, sint blanditiis sunt ratione saepe? Iusto ipsum illum enim aliquam quam maiores animi quidem? Odit, saepe suscipit eligendi obcaecati nam animi similique autem, soluta voluptatibus earum in, quasi qui exercitationem eum? Ipsa nisi, facere id aperiam reprehenderit aut eos odio sint est, pariatur voluptatem! At officiis, ex odit rem repudiandae autem laborum earum soluta rerum accusantium obcaecati ullam eius quos velit cupiditate recusandae! Facere excepturi architecto consequuntur, cupiditate, molestiae consequatur repellat reprehenderit molestias non quis sunt commodi, rerum facilis repellendus delectus fugit fuga? Aut id, dolor unde eveniet beatae temporibus iste, libero, optio tempore iusto facilis corporis illum rem doloremque iure aperiam ullam odio neque earum eaque reiciendis voluptas! Minima possimus quis illum laboriosam rerum consequatur mollitia, cumque nobis similique. Voluptatibus, et quaerat alias, unde debitis similique fuga, fugiat reiciendis inventore est beatae harum? Quo assumenda animi eos quibusdam reiciendis accusamus. Dolorum, ad! Libero nihil esse laborum eum nulla dicta illo veritatis, laboriosam explicabo blanditiis nam iusto eos maiores voluptates? Saepe eligendi, aliquid veritatis reiciendis vel placeat. Ex ipsa facilis illum asperiores labore suscipit mollitia laboriosam atque pariatur nihil saepe unde hic alias nostrum nulla, minus, illo ipsum deserunt officia ad soluta libero repellat dolorum. Voluptas iste hic blanditiis molestias! Consectetur autem ea, magni maxime vitae delectus hic. Et provident libero quisquam enim modi assumenda eveniet perspiciatis error corrupti. Enim aliquid suscipit molestias ratione nam! Facilis minima et illum delectus iste. Optio officiis aliquam earum molestiae reiciendis recusandae illo quia non, tenetur aperiam adipisci impedit autem quam quasi iste harum incidunt quibusdam odit architecto sint repellendus, assumenda maxime possimus placeat. Repudiandae dolorem illo, voluptatibus quibusdam ratione quasi recusandae fugiat atque! Nihil maiores fugiat natus molestiae iste magni tenetur optio vitae illo? Beatae fuga recusandae cum. Ad fuga soluta et earum. Cupiditate ad et molestias ipsam dolores minima deserunt numquam nisi totam sequi praesentium itaque explicabo at voluptate, repudiandae eum tempore illo iste dolorum est debitis dolorem. Reprehenderit nobis hic asperiores minima quo laudantium maxime fugit, commodi iure deserunt officia! Nisi nihil esse nesciunt quibusdam minus deleniti rem optio eum, iusto eaque autem fuga quod delectus doloremque sint, alias et, fugiat sapiente officia? Fugiat quia, consectetur non a blanditiis itaque rem vero architecto doloremque voluptatem, quibusdam sapiente temporibus iusto consequuntur. Animi ipsum voluptatibus suscipit, ipsam facilis odit eaque quis quos accusamus ducimus est, similique veritatis nostrum necessitatibus quisquam, perspiciatis unde ad iure aspernatur libero doloremque numquam in? Voluptatem veniam facere beatae! Quas repellendus vero repellat accusamus tempora, quisquam maiores vitae molestias. Accusantium nemo culpa hic saepe reprehenderit vitae porro, consectetur sapiente suscipit beatae ipsam. Tempora voluptatum cumque dignissimos fugiat tempore nesciunt vitae quasi ipsa id reiciendis harum qui, nostrum et eveniet recusandae provident sequi magni, aliquid alias animi culpa quia autem quibusdam. Inventore dolorem amet a maxime qui voluptas ullam, consequuntur exercitationem facilis tempora consectetur molestiae ex quo sed quidem, repellendus commodi quibusdam doloribus autem iure? Sapiente perferendis voluptatum assumenda ea! Cupiditate ducimus similique hic quidem, deleniti molestias eos eveniet libero a esse quaerat asperiores ipsam autem temporibus error minus expedita! Tenetur inventore facere nulla assumenda eos id in consequuntur at impedit? Sapiente, provident, accusantium distinctio quisquam commodi cumque eos hic facilis temporibus beatae voluptas sunt id quaerat quasi perferendis veritatis? Reprehenderit modi doloremque dolore distinctio fuga at, doloribus nostrum maxime commodi perspiciatis tenetur optio cumque eos nobis maiores sunt, velit veritatis nemo vero laudantium. Vero laborum excepturi rem reiciendis eligendi? Ad, molestiae nihil nisi earum voluptatem a commodi sunt quia dignissimos libero necessitatibus, deleniti nam laudantium autem dolor fugiat mollitia fuga inventore quam minima enim quasi? Sapiente, quasi animi quibusdam, tempore aspernatur earum mollitia voluptates libero laborum sunt culpa harum.',
            'assigned_user_id' => $userOne->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['text']));

        //case 7 - assigned_user_id fields is required
        $response = $this->json('PUT', '/api/me/tasks/' . $userOneTask->id . '/update', [
            'name' => 'name',
            'text' => 'text',
            // 'assigned_user_id' => $user->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['assigned_user_id']));

        //case 8 - assigned_user_id fields needs to be exist in our table
        $response = $this->json('PUT', '/api/me/tasks/' . $userOneTask->id . '/update', [
            'name' => 'name',
            'text' => 'text',
            'assigned_user_id' => 1111,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['assigned_user_id']));

        //case 8 - deadline fields is required
        $response = $this->json('PUT', '/api/me/tasks/' . $userOneTask->id . '/update', [
            'name' => 'name',
            'text' => 'text',
            'assigned_user_id' => $userOne->id,
            // 'deadline' => date("Y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['deadline']));


        //case 9 - deadline field format is wrong
        $response = $this->json('PUT', '/api/me/tasks/' . $userOneTask->id . '/update', [
            'name' => 'name',
            'text' => 'text',
            'assigned_user_id' => $userOne->id,
            'deadline' => date("y-m-d\TH:i", strtotime("+ 1 day")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['deadline']));

        //case 10 - deadline needs to be min one hour after current date time
        $response = $this->json('PUT', '/api/me/tasks/' . $userOneTask->id . '/update', [
            'name' => 'name',
            'text' => 'text',
            'assigned_user_id' => $userOne->id,
            'deadline' => date("Y-m-d\TH:i", strtotime("+ 10 minute")),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['deadline']));

        //case 11 - it should create new task
        $deadline = date("Y-m-d\TH:i", strtotime("+ 2 Hour"));
        $response = $this->json('PUT', '/api/me/tasks/' . $userOneTask->id . '/update', [
            'name' => 'name different',
            'text' => 'text different',
            'assigned_user_id' => $userOne->id,
            'deadline' => $deadline,
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('tasks', [
            'name' => 'name different',
            'text' => 'text different',
            'assigned_user_id' => $userOne->id,
            'deadline' => date("Y-m-d H:i:s", strtotime($deadline)),
        ]);

        //check that task can not be updated by author if it's status is in progress or done
        $t = Task::where("id", $userOneTask->id)->first();
        $t->status = Task::STATUS_IN_PROGRESS;
        $t->save();

        $deadline = date("Y-m-d\TH:i", strtotime("+ 2 Hour"));
        $response = $this->json('PUT', '/api/me/tasks/' . $userOneTask->id . '/update', [
            'name' => 'name different',
            'text' => 'text different',
            'assigned_user_id' => $userOne->id,
            'deadline' => $deadline,
        ]);

        $response->assertStatus(403);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals(ErrorMessage::TASK_CAN_NOT_BE_EDITED, $data);
    }

    /** @test */
    public function check_update_task_status_validation()
    {
        //login as jwt user
        $userOne = $this->jwtUser();

        $userSecond = $this->users()->first();

        //create task for user second and check that user one cannot update task status
        $userSecondTask = $this->tasks(['assigned_user_id' => $userSecond->id, 'status' => Task::STATUS_TO_DO])->first();
        $userOneTask = $this->tasks(['assigned_user_id' => $userOne->id, 'status' => Task::STATUS_TO_DO])->first();

        $response = $this->json('PUT', '/api/tasks/' . $userSecondTask->id . '/update/status', [
            'status' => Task::STATUS_IN_PROGRESS,
        ]);

        $response->assertStatus(403);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals(ErrorMessage::TASK_IS_NOT_ASSIGNED_TO_USER, $data);

        //status is null
        $response = $this->json('PUT', '/api/tasks/' . $userOneTask->id . '/update/status', [
            'status' => null,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals(ErrorMessage::INVALID_STATUS_VALUE, $data);
        //status exists but not valid
        $response = $this->json('PUT', '/api/tasks/' . $userOneTask->id . '/update/status', [
            'status' => 'aaa',
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals(ErrorMessage::INVALID_STATUS_VALUE, $data);

        //user cannot update status from todo to done
        $response = $this->json('PUT', '/api/tasks/' . $userOneTask->id . '/update/status', [
            'status' => Task::STATUS_DONE
        ]);

        $response->assertStatus(403);

        $data = json_decode($response->getContent(), true);
        $this->assertEquals(ErrorMessage::INVALID_STATUS_FROM_TODO_TO_DONE, $data);

        //user cannot update status from in progress to to do
        //first update task's status
        $response = $this->json('PUT', '/api/tasks/' . $userOneTask->id . '/update/status', [
            'status' => Task::STATUS_IN_PROGRESS
        ]);

        $response->assertStatus(200);

        $response = $this->json('PUT', '/api/tasks/' . $userOneTask->id . '/update/status', [
            'status' => Task::STATUS_TO_DO
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals(ErrorMessage::INVALID_STATUS_VALUE, $data);


        //it should not update if existing task status and status from reques is same
        $response = $this->json('PUT', '/api/tasks/' . $userOneTask->id . '/update/status', [
            'status' => Task::STATUS_IN_PROGRESS
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals(ErrorMessage::CURRENT_STATUS_IS_SAME, $data);


        //it should not update task's status if it is already done

        //first update status as done
        $response = $this->json('PUT', '/api/tasks/' . $userOneTask->id . '/update/status', [
            'status' => Task::STATUS_DONE
        ]);

        $response->assertStatus(200);

        $response = $this->json('PUT', '/api/tasks/' . $userOneTask->id . '/update/status', [
            'status' => Task::STATUS_IN_PROGRESS
        ]);

        $response->assertStatus(403);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals(ErrorMessage::TASK_STATUS_IS_DONE_AND_CAN_NOT_BE_CHANGED, $data);
    }

    /** @test */
    public function check_delete_task_endpoint()
    {
        //login as jwt user
        $userOne = $this->jwtUser();

        $userSecond = $this->users()->first();

        //create task for user second and check that user one cannot delete task
        $userSecondTask = $this->tasks(['author_id' => $userSecond->id, 'status' => Task::STATUS_TO_DO])->first();
        $userOneTask = $this->tasks(['author_id' => $userOne->id, 'status' => Task::STATUS_TO_DO])->first();

        $response = $this->json('DELETE', '/api/me/tasks/' . $userSecondTask->id . '/delete', []);

        $response->assertStatus(403);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals(ErrorMessage::TASK_DOES_NOT_BELONGS_TO_USER, $data);

        //check det if task's status is in pgoress it can not be deleted
        $userOneTask->status = Task::STATUS_IN_PROGRESS;
        $userOneTask->save();

        $response = $this->json('DELETE', '/api/me/tasks/' . $userOneTask->id . '/delete', []);

        $response->assertStatus(403);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals(ErrorMessage::TASK_CAN_NOT_BE_DELETED, $data);

        //check taht if task is done it can be deleted
        $userOneTask->status = Task::STATUS_DONE;
        $userOneTask->save();

        $response = $this->json('DELETE', '/api/me/tasks/' . $userOneTask->id . '/delete', []);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('tasks', [
            'id' => $userOneTask->id,
        ]);

        //check that task which status is todo can be also deleted
        $userOneTask = $this->tasks(['author_id' => $userOne->id, 'status' => Task::STATUS_TO_DO])->first();

        $response = $this->json('DELETE', '/api/me/tasks/' . $userOneTask->id . '/delete', []);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('tasks', [
            'id' => $userOneTask->id,
        ]);
    }
}
