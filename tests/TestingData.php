<?php
namespace Tests;

use Tymon\JWTAuth\Facades\JWTAuth;

trait TestingData
{

    public function users($params = [], $count = 1)
    {
        return  \App\Models\User::factory($count)->create($params);
    }

    public function tasks($params = [], $count = 1)
    {
        return  \App\Models\Task::factory($count)->create($params);
    }

    public function jwtUser()
    {
        $user = $this->users()->first();
        $token = JWTAuth::fromUser($user);
        $this->withHeader('Authorization', "Bearer {$token}");
        parent::actingAs($user);

        return $user;
    }
}
