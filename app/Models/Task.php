<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    public const STATUS_TO_DO = 1;
    public const STATUS_IN_PROGRESS = 2;
    public const STATUS_DONE = 3;

    public const SORT_ARRAY = [
        'deadline' => 'Deadline +',
        'deadline-' => 'Deadline -',
        'created_at' => 'Created At +',
        'created_at-' => 'Created At -',
    ];

    public const STATUS_ARRAY = [
        self::STATUS_TO_DO => 'To do',
        self::STATUS_IN_PROGRESS => 'In progress',
        self::STATUS_DONE => 'Done',
    ];


    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function assignedUser()
    {
        return $this->belongsTo(User::class, 'assigned_user_id');
    }

    public function canBeEdited()
    {
        return $this->status == self::STATUS_TO_DO ? true : false;
    }

    public function canBeDeleted()
    {
        return $this->status == self::STATUS_DONE || $this->status == self::STATUS_TO_DO ? true : false;
    }

    public function getCreatedAt()
    {
        return date('Y-m-d H:i:s', strtotime($this->created_at));
    }
}
