<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\TaskRequest;

class HomeController extends Controller
{

    public function index()
    {
        return view('home');
    }
}
