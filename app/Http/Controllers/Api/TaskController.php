<?php

namespace App\Http\Controllers\Api;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Http\Requests\TaskRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\TaskResource;
use App\Services\ErrorMessage;
use Illuminate\Validation\ValidationException;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        $objects = Task::with(['author', 'assignedUser']);

        if ($request->sort == 'deadline') {
            $objects->orderBy('deadline', 'desc');
        }
        if ($request->sort == 'deadline-') {
            $objects->orderBy('deadline', 'asc');
        }

        if ($request->sort == 'created_at') {
            $objects->orderBy('created_at', 'desc');
        }
        if ($request->sort == 'created_at-') {
            $objects->orderBy('created_at', 'asc');
        }

        if (!empty($request->status)) {
            $objects->where('status', $request->status);
        };

        if (!empty($request->keyword)) {
            $objects->where('id', 'like', '%' . $request->keyword . '%')
            ->orWhere('name', 'like', '%' . $request->keyword  . '%');
        };

        $objects = $objects->orderBy('status')->paginate(10);


        return TaskResource::collection($objects);
    }

    public function show(Task $object)
    {
        return new TaskResource($object);
    }

    public function createdTasks(Request $request)
    {

        $user = auth()->user();

        $objects = Task::where('author_id', $user->id)->with(['author', 'assignedUser']);

        if ($request->sort == 'deadline') {
            $objects->orderBy('deadline', 'desc');
        }
        if ($request->sort == 'deadline-') {
            $objects->orderBy('deadline', 'asc');
        }

        if ($request->sort == 'created_at') {
            $objects->orderBy('created_at', 'desc');
        }
        if ($request->sort == 'created_at-') {
            $objects->orderBy('created_at', 'asc');
        }

        if (!empty($request->status)) {
            $objects->where('status', $request->status);
        };

        if (!empty($request->keyword)) {
            $objects->where('id', 'like', '%' . $request->keyword . '%')
            ->orWhere('name', 'like', '%' . $request->keyword  . '%');
        };

        $objects = $objects->orderBy('status')->paginate(10);

        return TaskResource::collection($objects);
    }

    public function assignedTasks(Request $request)
    {
        $user = auth()->user();

        $objects = Task::where('assigned_user_id', $user->id)->with(['author', 'assignedUser']);

        if ($request->sort == 'deadline') {
            $objects->orderBy('deadline', 'desc');
        }
        if ($request->sort == 'deadline-') {
            $objects->orderBy('deadline', 'asc');
        }

        if ($request->sort == 'created_at') {
            $objects->orderBy('created_at', 'desc');
        }
        if ($request->sort == 'created_at-') {
            $objects->orderBy('created_at', 'asc');
        }

        if (!empty($request->status)) {
            $objects->where('status', $request->status);
        };

        if (!empty($request->keyword)) {
            $objects->where('id', 'like', '%' . $request->keyword . '%')
            ->orWhere('name', 'like', '%' . $request->keyword  . '%');
        };

        $objects = $objects->orderBy('status')->paginate(10);

        return TaskResource::collection($objects);
    }

    public function create(TaskRequest $request)
    {
        $deadline = date("Y-m-d H:i:s", strtotime($request->deadline));
        //get current deadline and check if it is min one hour future
        $validDate = date("Y-m-d H:i:s", strtotime("+ 1 Hour"));
        if (strtotime($deadline) < strtotime($validDate)) {
            throw ValidationException::withMessages(['deadline' => 'Deadline needs to be minimum one hour from now']);
        }

        $user = auth()->user();
        $object = new Task();
        $object->author_id = $user->id;
        $object->assigned_user_id = $request->assigned_user_id;
        $object->name = $request->name;
        $object->text = $request->text;
        $object->deadline = $deadline;
        $object->save();

        return new TaskResource($object);
    }

    public function update(TaskRequest $request, Task $object)
    {
        $deadline = date("Y-m-d H:i:s", strtotime($request->deadline));
        //get current deadline and check if it is min one hour future
        $validDate = date("Y-m-d H:i:s", strtotime("+ 1 Hour"));
        if (strtotime($deadline) < strtotime($validDate)) {
            throw ValidationException::withMessages(['deadline' => 'Deadline needs to be minimum one hour from now']);
        }

        //check if task belongs to logged in user
        $user = auth()->user();

        if ($object->author_id != $user->id) {
            return response()->json(ErrorMessage::TASK_DOES_NOT_BELONGS_TO_USER, 403);
        }
        //check status
        if (!$object->canBeEdited()) {
            return response()->json(ErrorMessage::TASK_CAN_NOT_BE_EDITED, 403);
        }

        $deadline = date("Y-m-d H:i:s", strtotime($request->deadline));


        $object->assigned_user_id = $request->assigned_user_id;
        $object->name = $request->name;
        $object->text = $request->text;
        $object->deadline = $deadline;
        $object->save();

        return new TaskResource($object);
    }

    public function delete(Task $object)
    {
        //check if task belongs to logged in user
        $user = auth()->user();
        if ($object->author_id != $user->id) {
            return response()->json(ErrorMessage::TASK_DOES_NOT_BELONGS_TO_USER, 403);
        }

        if (!$object->canBeDeleted()) {
            return response()->json(ErrorMessage::TASK_CAN_NOT_BE_DELETED, 403);
        }

        $object->delete();

        return response()->json("Task Deleted Successfully");
    }

    public function updateStatus(Request $request, Task $object)
    {
        $user = auth()->user();

        //check if user can change status
        if ($user->id != $object->assigned_user_id) {
            return response()->json(ErrorMessage::TASK_IS_NOT_ASSIGNED_TO_USER, 403);
        }

        //validate status values
        $allowed_values = [Task::STATUS_IN_PROGRESS, Task::STATUS_DONE];

        if (!in_array($request->status, $allowed_values)) {
            return response()->json(ErrorMessage::INVALID_STATUS_VALUE, 422);
        }

        if ($object->status == $request->status) {
            return response()->json(ErrorMessage::CURRENT_STATUS_IS_SAME, 422);
        }

        //if status is already done forbid
        if ($object->status == Task::STATUS_DONE) {
            return response()->json(ErrorMessage::TASK_STATUS_IS_DONE_AND_CAN_NOT_BE_CHANGED, 403);
        }

        //only allow from todo => in progress
        if ($object->status == Task::STATUS_TO_DO && $request->status != Task::STATUS_IN_PROGRESS) {
            return response()->json(ErrorMessage::INVALID_STATUS_FROM_TODO_TO_DONE, 403);
        }


        $object->status = $request->status;
        $object->save();

        return new TaskResource($object);
    }
}
