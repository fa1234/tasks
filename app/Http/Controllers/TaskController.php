<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\TaskRequest;
use Illuminate\Validation\ValidationException;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $objects = Task::with(['author', 'assignedUser']);

        if ($request->sort == 'deadline') {
            $objects->orderBy('deadline', 'desc');
        }
        if ($request->sort == 'deadline-') {
            $objects->orderBy('deadline', 'asc');
        }

        if ($request->sort == 'created_at') {
            $objects->orderBy('created_at', 'desc');
        }
        if ($request->sort == 'created_at-') {
            $objects->orderBy('created_at', 'asc');
        }

        if (!empty($request->status)) {
            $objects->where('status', $request->status);
        };

        if (!empty($request->keyword)) {
            $keyword = $request->keyword;
            $objects->where(function ($q) use ($keyword) {
                $q->where('id', 'like', '%' . $keyword . '%')
                ->orWhere('name', 'like', '%' . $keyword  . '%');
            });
        };

        $objects = $objects->orderBy('status')->paginate(10);


        return view('tasks.index', compact('objects'));
    }

    public function assigned(Request $request)
    {
        $objects = Task::where('assigned_user_id', auth()->user()->id)->with(['author', 'assignedUser']);

        if ($request->sort == 'deadline') {
            $objects->orderBy('deadline', 'desc');
        }
        if ($request->sort == 'deadline-') {
            $objects->orderBy('deadline', 'asc');
        }

        if ($request->sort == 'created_at') {
            $objects->orderBy('created_at', 'desc');
        }
        if ($request->sort == 'created_at-') {
            $objects->orderBy('created_at', 'asc');
        }

        if (!empty($request->status)) {
            $objects->where('status', $request->status);
        };

        if (!empty($request->keyword)) {
            $keyword = $request->keyword;
            $objects->where(function ($q) use ($keyword) {
                $q->where('id', 'like', '%' . $keyword . '%')
                ->orWhere('name', 'like', '%' . $keyword  . '%');
            });
        };

        $objects = $objects->orderBy('status')->paginate(10);

        return view('tasks.assigned', compact('objects'));
    }

    public function created(Request $request)
    {
        $user = auth()->user();

        $objects = Task::where('author_id', $user->id)->with(['author', 'assignedUser']);

        if ($request->sort == 'deadline') {
            $objects->orderBy('deadline', 'desc');
        }
        if ($request->sort == 'deadline-') {
            $objects->orderBy('deadline', 'asc');
        }

        if ($request->sort == 'created_at') {
            $objects->orderBy('created_at', 'desc');
        }
        if ($request->sort == 'created_at-') {
            $objects->orderBy('created_at', 'asc');
        }

        if (!empty($request->status)) {
            $objects->where('status', $request->status);
        };

        if (!empty($request->keyword)) {
            $keyword = $request->keyword;
            $objects->where(function ($q) use ($keyword) {
                $q->where('id', 'like', '%' . $keyword . '%')
                ->orWhere('name', 'like', '%' . $keyword  . '%');
            });
        };

        $objects = $objects->orderBy('status')->paginate(10);

        return view('tasks.created', compact('objects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();

        return view('tasks.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskRequest $request)
    {
        $deadline = date("Y-m-d H:i:s", strtotime($request->deadline));
        //get current deadline and check if it is min one hour future
        $validDate = date("Y-m-d H:i:s", strtotime("+ 1 Hour"));
        if (strtotime($deadline) < strtotime($validDate)) {
            throw ValidationException::withMessages(['deadline' => 'Deadline needs to be minimum one hour from now']);
        }

        $user = auth()->user();
        $task = new Task();
        $task->author_id = $user->id;
        $task->assigned_user_id = $request->assigned_user_id;
        $task->name = $request->name;
        $task->text = $request->text;
        $task->deadline = $deadline;
        $task->save();

        return redirect()->back()->with('success', "Task Created Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $object)
    {
      //check if task belongs to logged in user
        $user = auth()->user();
        $users = User::all();
        if ($object->author_id != $user->id) {
            return redirect()->back()->with('error', "Task Does not belongs to you");
        }

        return view('tasks.edit', compact('object', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(TaskRequest $request, Task $object)
    {
        //check if task belongs to logged in user
        $user = auth()->user();

        if ($object->author_id != $user->id) {
            return redirect()->back()->with('error', "Task Does not belongs to you");
        }
        //check status
        if (!$object->canBeEdited()) {
            return redirect()->back()->with('error', "Task Can't be edited anymore");
        }

        $deadline = date("Y-m-d H:i:s", strtotime($request->deadline));
        //get current deadline and check if it is min one hour future
        $validDate = date("Y-m-d H:i:s", strtotime("+ 1 Hour"));
        if (strtotime($deadline) < strtotime($validDate)) {
            throw ValidationException::withMessages(['deadline' => 'Deadline needs to be minimum one hour from now']);
        }

        $user = auth()->user();

        $object->assigned_user_id = $request->assigned_user_id;
        $object->name = $request->name;
        $object->text = $request->text;
        $object->deadline = $deadline;
        $object->save();

        return redirect()->back()->with('success', "Task Updated Successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $object)
    {
        //check if task belongs to logged in user
        $user = auth()->user();
        if ($object->author_id != $user->id) {
            return redirect()->back()->with('error', "Task Does not belongs to you");
        }

        if (!$object->canBeDeleted()) {
            return redirect()->back()->with('error', "Task Can't be deleted if it's status is not Done or To do");
        }

        $object->delete();


        return redirect()->back()->with('success', "Task Deleted Successfully");
    }


    public function updateStatus(Request $request, Task $object)
    {
        $user = auth()->user();

        //check if user can change status
        if ($user->id != $object->assigned_user_id) {
            return redirect()->back()->with('error', "You don't have permission to change current task's status");
        }

        //validate status values
        $allowed_values = [Task::STATUS_IN_PROGRESS, Task::STATUS_DONE];

        if (!in_array($request->status, $allowed_values)) {
            return redirect()->back()->with('error', "Invalid status value");
        }
        //if status is already done forbid
        if ($object->status == Task::STATUS_DONE) {
            return redirect()->back()->with('error', "You can't change task which status is already Done");
        }

        //only allow from todo => in progress
        if ($object->status == Task::STATUS_TO_DO && $request->status != Task::STATUS_IN_PROGRESS) {
            return redirect()->back()->with('error', "You only can change task status from To do to In progress");
        }

        $object->status = $request->status;
        $object->save();

        return redirect()->back()->with('success', "Status Changed Successfully");
    }
}
