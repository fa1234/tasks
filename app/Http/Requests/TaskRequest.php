<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //mark hour min 1 hour in future

        return [
            'name' => 'required|min:2|max:255',
            'text' => 'required|min:2|max:1000',
            'assigned_user_id' => 'required|exists:users,id',
            'deadline' => 'required|date_format:Y-m-d\TH:i',
        ];
    }
}
