<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'text' => $this->text,
            'status' => $this->status,
            'created_at' => $this->getCreatedAt(),
            'deadline' => $this->deadline,
            'author' => $this->author ? new UserResource($this->author) : null,
            'assigned_user' => $this->author ? new UserResource($this->assignedUser) : null,
        ];
    }
}
