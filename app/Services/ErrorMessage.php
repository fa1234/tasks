<?php
namespace App\Services;

class ErrorMessage
{

    const TASK_DOES_NOT_BELONGS_TO_USER = 'task does not belongs to you';
    const TASK_IS_NOT_ASSIGNED_TO_USER = 'task is not assigned to you';
    const INVALID_STATUS_VALUE = 'Invalid status value';
    const INVALID_STATUS_FROM_TODO_TO_DONE = "You only can change task status from To do to In progress";
    const CURRENT_STATUS_IS_SAME = "Task's current status is same";
    const TASK_STATUS_IS_DONE_AND_CAN_NOT_BE_CHANGED = "You can't change task which status is already Done";
    const TASK_CAN_NOT_BE_EDITED = "Task Can't be edited anymore";
    const TASK_CAN_NOT_BE_DELETED = "Task Can't be deleted if it's status is not Done";
}
