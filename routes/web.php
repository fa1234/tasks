<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes(['register' => false]);


Route::middleware(['auth:web'])->group(function () {

    Route::get('/tasks', [TaskController::class, 'index'])->name('tasks');
    Route::get('/me/tasks', [TaskController::class, 'index'])->name('tasks');








    Route::get('/me', [AuthController::class, 'userProfile']);

    //tasks
    Route::get('/tasks', [TaskController::class, 'index'])->name('tasks.index');
    Route::get('/tasks/created', [TaskController::class, 'created'])->name('tasks.created');
    Route::get('/tasks/assigned', [TaskController::class, 'assigned'])->name('tasks.assigned');
    Route::get('/tasks/crete', [TaskController::class, 'create'])->name('tasks.create');
    Route::post('/tasks/crete', [TaskController::class, 'store'])->name('tasks.store');
    Route::get('/tasks/{object}/edit', [TaskController::class, 'edit'])->name('tasks.edit');
    Route::put('/tasks/{object}/update', [TaskController::class, 'update'])->name('tasks.update');
    Route::delete('/tasks/{object}/delete', [TaskController::class, 'destroy'])->name('tasks.destroy');
    Route::post('/tasks/{object}/status', [TaskController::class, 'updateStatus'])->name('tasks.updateStatus');
});
