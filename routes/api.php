<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\TaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/login', [AuthController::class, 'login']);

Route::middleware(['auth:api'])->group(function () {


    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/me', [AuthController::class, 'userProfile']);

    //tasks
    Route::get('/tasks', [TaskController::class, 'index']);
    Route::get('/tasks/{object}/show', [TaskController::class, 'show']);
    Route::get('/me/tasks/created', [TaskController::class, 'createdTasks']);
    Route::get('/me/tasks/assigned', [TaskController::class, 'assignedTasks']);
    Route::post('/me/tasks/crete', [TaskController::class, 'create']);
    Route::put('/me/tasks/{object}/update', [TaskController::class, 'update']);
    Route::put('/tasks/{object}/update/status', [TaskController::class, 'updateStatus']);
    Route::delete('/me/tasks/{object}/delete', [TaskController::class, 'delete']);
});
