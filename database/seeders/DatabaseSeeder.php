<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        //create admin user
        \App\Models\User::factory(1)->create(['username' => 'admin']);


        $users = User::all();

        foreach ($users as $user) {
            $otherUser = $users->random(1)->first();
            //assigned tasks
            \App\Models\Task::factory(5)->create(['assigned_user_id' => $user->id, 'author_id' => $otherUser->id]);

            //authored tasks
            \App\Models\Task::factory(5)->create(['author_id' => $user->id, 'assigned_user_id' => $otherUser->id,]);
        }
    }
}
