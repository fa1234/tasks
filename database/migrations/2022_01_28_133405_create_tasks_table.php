<?php

use App\Models\Task;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('author_id');
            $table->unsignedBigInteger('assigned_user_id');
            $table->string('name');
            $table->text('text');
            $table->enum('status', [Task::STATUS_TO_DO, Task::STATUS_IN_PROGRESS, Task::STATUS_DONE])->default(Task::STATUS_TO_DO);
            $table->dateTime('deadline');
            $table->timestamps();

            $table->foreign('author_id')
            ->references('id')
            ->on('users')
            ->onDelete('RESTRICT');

            $table->foreign('assigned_user_id')
            ->references('id')
            ->on('users')
            ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
