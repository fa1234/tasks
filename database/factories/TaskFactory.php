<?php

namespace Database\Factories;

use App\Models\Task;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $statuses = [Task::STATUS_TO_DO, Task::STATUS_IN_PROGRESS, Task::STATUS_DONE];
        $status = $statuses[array_rand($statuses)];
        $deadline_hour_ranges = [10, 5, 7, 3];
        $hour = $deadline_hour_ranges[array_rand($deadline_hour_ranges)];


        if ($status == Task::STATUS_TO_DO || $status == Task::STATUS_IN_PROGRESS) {
            $deadline = date("Y-m-d H", strtotime("+ $hour Hour"));
            $created_at = date("Y-m-d H");
        } else {
            $deadline = date("Y-m-d H", strtotime("- $hour Hour"));
            $h = ($hour * 2);
            $created_at = date("Y-m-d H", strtotime("- $h Hour"));
        }


        return [
            'author_id' => function () {
                return \App\Models\User::factory(1)->create()->first()->id;
            },
            'assigned_user_id' => function () {
                return \App\Models\User::factory(1)->create()->first()->id;
            },
            'name' => $this->faker->paragraph(1),
            'text' => $this->faker->paragraph(5),
            'status' => $status,
            'deadline' => $deadline . ":00:00",
            'created_at' => $created_at . ":00:00"
        ];
    }
}
