@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <h4 class="mb-2">Number Of records: {{ $objects->total() }}</h4>
            @include('components.search')


            @foreach ($objects as $object)

            <div class="card">
                <div class="card-header">{{ $object->name }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ $object->text }}
                </div>
                <div class="card-footer">
                    <ul>
                        <li>Assigned User Username: {{ $object->assignedUser->username }}</li>
                        <li>Created At: {{ $object->created_at }}</li>
                        <li>Deadline: {{ $object->deadline }}</li>
                        <li>
                            Status:
                            @if($object->status == \App\Models\Task::STATUS_TO_DO)
                                <span class="btn-sm btn-info">To do</span>
                            @elseif($object->status == \App\Models\Task::STATUS_IN_PROGRESS)
                                <span class="btn-sm btn-warning">In progress</span>

                            @elseif($object->status == \App\Models\Task::STATUS_DONE)
                                <span class="btn-sm btn-success">Done</span>
                            @else
                                Unknown status
                            @endif
                        </li>
                    </ul>

                    <a class="btn-sm btn-dark" href="{{ route('tasks.edit', $object) }}">Edit Task</a>

                    <form action="{{ route('tasks.destroy', $object) }}" method="POST" class="mt-3"  onSubmit="return confirm('Are you sure you wish to delete?');">
                        @csrf
                        @method('delete')
                        <button class="btn-sm btn-danger">Delete</button>
                    </form>

                </div>
            </div>
            <br>

            @endforeach

            {{ $objects->links('vendor.pagination.bootstrap-4') }}

        </div>
    </div>
</div>
@endsection
