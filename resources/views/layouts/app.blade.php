<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @yield('css')
    <style>
        #alertContainer{
            position: fixed;
            bottom: 10px;
            right: 10px;
            z-index: 1;
        }
    </style>
</head>
<body>

    @if(session('error') || (session('success')))
    <div id="alertContainer">
        <div class="alert alert-{{ session('error') ? 'danger' : 'success' }} alert-dismissible fade show" role="alert">
            <strong>{{ session('error') ? session('error') : session('success') }}
          </div>
    </div>
    @endif
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                @php
                        //get first slug from url
                        if(Request::segment(2)){
                            $slug = Request::segment(2);
                        }else{
                            $slug = Request::segment(1);
                        }
                @endphp


                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    @if(auth()->check())
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">


                        <li class="nav-item active">
                            <a class="nav-link {{ $slug == 'tasks' ? 'active' : '' }}" href="{{ route('tasks.index') }}">Tasks</span></a>
                        </li>

                        <li class="nav-item active">
                            <a class="nav-link {{ @$slug == 'assigned' ? 'active' : '' }}" href="{{ route('tasks.assigned') }}">Tasks assigned to me</span></a>
                        </li>


                        <li class="nav-item active">
                            <a class="nav-link {{ @$slug == 'created' ? 'active' : '' }}" href="{{ route('tasks.created') }}">Created tasks</span></a>
                        </li>

                        <li class="nav-item active">
                            <a class="nav-link {{ @$slug == 'crete' ? 'active' : '' }}" href="{{ route('tasks.create') }}">Create new task {{ $slug }}</span></a>
                        </li>
                    </ul>
                    @endif

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>



        <main class="py-4">
            @yield('content')


        </main>
    </div>

    <script>

window.addEventListener('DOMContentLoaded', (event) => {
    var element =  document.getElementById('alertContainer');
    if (typeof(element) != 'undefined' && element != null)
    {
        setTimeout(function(){
            element.style.display = "none";
        }, 3000);
            }
});

    </script>

    @yield('js')
</body>
</html>
