<div class="card">
    <div class="card-body">
        <form method="get" action="">
        <div class="row mb-4">
            <div class="col-md-4">
                <input id="keyword" type="text" class="form-control" name="keyword" placeholder="keyword" value="{{ request('keyword') }}">
            </div>

            <div class="col-md-4">
                <select class="form-select" name="status">
                    <option value="">Select Status</option>
                        @foreach (\App\Models\Task::STATUS_ARRAY as $key =>$status)
                            <option value="{{ $key }}" {{ request('status') == $key ? 'selected' : '' }}>{{ $status }}</option>
                        @endforeach
                  </select>
            </div>

            <div class="col-md-4">
                <select class="form-select" name="sort">
                    <option>Sort</option>
                    @foreach (\App\Models\Task::SORT_ARRAY as $key =>$sort)
                    <option value="{{ $key }}" {{ request('sort') == $key ? 'selected' : '' }}>{{ $sort }}</option>
                    @endforeach
                  </select>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary">
                    Search
                </button>
            </div>

            <div class="col-md-2">
                <a class="btn btn-dark" href="{{ Request::url() }}">Clear</a>
            </div>
        </div>
    </form>
    </div>
</div>
<br>
